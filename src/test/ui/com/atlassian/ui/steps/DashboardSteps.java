package com.atlassian.ui.steps;

import com.atlassian.ui.pages.DashboardPage;
import com.atlassian.ui.utility.config.ResourceManager;
import cucumber.api.java.en.When;

public class DashboardSteps extends BaseSteps {

    public DashboardSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private DashboardPage dashboardPage = new DashboardPage(driver);

    @When("^I navigate to the project atlassian space$")
    public void i_Navigate_To_The_Project_Atlassian_Space() throws Throwable {
        dashboardPage.navigateToSpace();
        dashboardPage.assertPage();
    }

}

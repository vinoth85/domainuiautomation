package com.atlassian.ui.steps;

import com.atlassian.ui.pages.PublishPage;
import com.atlassian.ui.utility.config.ResourceManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import static junit.framework.TestCase.assertTrue;

public class PublishPageSteps extends BaseSteps {

    public PublishPageSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private PublishPage publishPage = new PublishPage(driver);

    @And("^I provide the title \"([^\"]*)\"$")
    public void i_Provide_The_Title(String pagetitle) throws Throwable {
        publishPage.getPageTitleElement().sendKeys(pagetitle);
    }

    @And("^I publish the page$")
    public void i_Publish_The_Page() throws Throwable {
        String expectTitle = "TestPage - Project Atlassian - Confluence";
        publishPage.getPublishButton().click();
        assertTrue(publishPage.getPublishedPageTitle().contains(expectTitle));
    }

    @And("^I validate the page \"([^\"]*)\" has created$")
    public void i_Validate_The_Page_Has_Created(String pageTitle) throws Throwable {
        publishPage.getPageTree(pageTitle);
    }

    @Then("^I delete the page$")
    public void i_Delete_The_Page() throws Throwable {
        String deleteTitle = "Delete Page - TestPage - Project Atlassian - Confluence";
        publishPage.getActionMenuLinkElement().click();
        publishPage.getActionRemoveElement().click();
        assertTrue(publishPage.getDeleteTitle().contains(deleteTitle));
        publishPage.getConfirmDeleteButton().click();
    }
}

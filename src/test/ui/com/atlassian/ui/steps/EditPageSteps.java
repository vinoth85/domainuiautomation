package com.atlassian.ui.steps;

import com.atlassian.ui.pages.PublishPage;
import com.atlassian.ui.utility.config.ResourceManager;
import com.atlassian.ui.pages.EditPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import static org.hamcrest.MatcherAssert.assertThat;

public class EditPageSteps extends BaseSteps {

    public EditPageSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private EditPage editPage = new EditPage(driver);
    private PublishPage publishPage = new PublishPage(driver);

    @And("^I open the page \"([^\"]*)\"$")
    public void i_Open_The_Page(String arg1) throws Throwable {
        editPage.getavailablePageLink().click();
    }

    @And("^I edit the restrictions$")
    public void i_Edit_The_Restrictions() throws Throwable {
        publishPage.getActionMenuLinkElement().click();
        editPage.getRestrictionsElement().click();
        editPage.getEditDialog();

    }

    @Then("^I set the restrictions \"([^\"]*)\" to the page for the user \"([^\"]*)\"$")
    public void i_Set_The_Restrictions(String restrictionType, String userName) throws Throwable {
        editPage.selectRestrictionType(restrictionType);
        editPage.getAccessText().contains("Has no access");
        editPage.getUserNameField().sendKeys(userName);
        editPage.selectUserName(userName);
        editPage.getAddButton().click();
        editPage.selectRestrictionType();
        editPage.getApplyButton().click();
    }

    @Then("^I confirm that the page is restricted$")
    public void i_Confirm_That_The_Page_Is_Restricted() throws Throwable {
        assertThat(String.valueOf(editPage.isElementPresent()), true);
    }
}

package com.atlassian.ui.steps;

import com.atlassian.ui.pages.LogOutPage;
import com.atlassian.ui.utility.config.ResourceManager;
import cucumber.api.java.en.And;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class LogOutSteps extends BaseSteps {

    public LogOutSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private LogOutPage logOutPage = new LogOutPage(driver);

    @And("^I logout$")
    public void i_Logout() throws Throwable {
        logOutPage.getLogOutLink().click();
        logOutPage.assertPage();
        logOutPage.getLogOutButton().click();
        driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
    }
}

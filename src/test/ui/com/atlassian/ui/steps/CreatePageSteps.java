package com.atlassian.ui.steps;

import com.atlassian.ui.pages.CreatePage;
import com.atlassian.ui.utility.config.ResourceManager;
import cucumber.api.java.en.And;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertTrue;

public class CreatePageSteps extends BaseSteps {

    public CreatePageSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private CreatePage createPage = new CreatePage(driver);

    @And("^I create a blank page$")
    public void i_Create_A_Blank_Page() throws Throwable {
        createPage.getPagesLink().click();
        createPage.getCreatePageButton().click();
        assertThat(createPage.getDialog().getText(), containsString("Create"));
        createPage.getBlankPageLink().click();
        createPage.getCreateDialogButton().click();
        String expectTitle = "Add Page - Project Atlassian - Confluence";
        assertTrue(createPage.getAddPageTitle().contains(expectTitle));
    }

}

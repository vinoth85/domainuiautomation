package com.atlassian.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogOutPage extends BasePage {

    public LogOutPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getLogOutLink() throws InterruptedException {
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".WebItemDropdownMenu_root_1eT .jGirHg")));
        Thread.sleep(3000);
        driver.findElement(By.cssSelector(".fJkvZT")).click();
        return driver.findElement(By.cssSelector("a[href*='/wiki/logout']"));
    }


    public WebElement getLogOutButton() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.id("logout-submit")));
        return driver.findElement(By.id("logout-submit"));
    }

    public void assertPage() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.titleIs("Log out - Atlassian account"));
    }
}

package com.atlassian.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPage extends BasePage {

    public EditPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getavailablePageLink() {
        driver.findElement(By.xpath(".//span[@class = 'KcgsE' and contains(text(),'Pages')]")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".link-title")));
        WebElement availablePageLink = driver.findElement(By.cssSelector(".link-title"));
        return availablePageLink;
    }

    public WebElement getRestrictionsElement() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#action-page-permissions-link")));
        WebElement restrictionsLink = driver.findElement(By.cssSelector("#action-page-permissions-link"));
        return restrictionsLink;
    }

    public void getEditDialog() {
        new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOfElementLocated(By.id("page-restrictions-dialog-selector")));
    }

    public void selectRestrictionType(String restrictionValue) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_page-restrictions-dialog-selector")));
        Select restrictionDropdown = new Select(driver.findElement(By.id("page-restrictions-dialog-selector")));
        restrictionDropdown.selectByVisibleText(restrictionValue);
    }

    public WebElement getUserNameField() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("s2id_autogen2")));
        WebElement enterUserName = driver.findElement(By.id("s2id_autogen2"));
        return enterUserName;
    }

    public void selectUserName(String userName) {
        driver.findElement(By.xpath(".//span[@data-username = '" + userName + "-restricteduser']")).click();
    }

    public WebElement getAddButton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("page-restrictions-add-button")));
        WebElement addButton = driver.findElement(By.id("page-restrictions-add-button"));
        return addButton;
    }

    public void selectRestrictionType() {
        Select restrictionType = new Select(driver.findElement(By.cssSelector(".select")));
        restrictionType.selectByVisibleText("Can view");
    }

    public String getAccessText() {
        String accessLevelText = driver.findElement(By.cssSelector(".page-restrictions-dialog-entities-container")).getText();
        return accessLevelText;
    }

    public WebElement getApplyButton() {
        WebElement applyButton = driver.findElement(By.cssSelector("#page-restrictions-dialog-save-button"));
        return applyButton;
    }

    public boolean isElementPresent() {
        try {
            return driver.findElement(By.cssSelector("#editPageLink")).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}



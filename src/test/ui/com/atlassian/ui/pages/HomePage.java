package com.atlassian.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void assertPage() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("Log in to continue - Atlassian account"));
    }
}


package com.atlassian.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PublishPage extends BasePage {

    public PublishPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getPageTitleElement() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("content-title")));
        return driver.findElement(By.id("content-title"));
    }

    public WebElement getPublishButton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".save-button-container")));
        return driver.findElement(By.cssSelector(".save-button-container"));

    }

    public String getPublishedPageTitle() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("TestPage - Project Atlassian - Confluence"));
        return driver.getTitle();

    }

    public void getPageTree(String pageTitle) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[href*='/TestPage']")));
        driver.findElement(By.cssSelector("a[href*='/TestPage']")).isDisplayed();
    }


    public WebElement getActionMenuLinkElement() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.id("action-menu-link")));
        return driver.findElement(By.id("action-menu-link"));
    }

    public WebElement getActionRemoveElement() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".action-remove")));
        return driver.findElement(By.cssSelector(".action-remove"));
    }

    public String getDeleteTitle() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("Delete Page - TestPage - Project Atlassian - Confluence"));
        return driver.getTitle();
    }

    public WebElement getConfirmDeleteButton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("confirm")));
        return driver.findElement(By.id("confirm"));
    }
}



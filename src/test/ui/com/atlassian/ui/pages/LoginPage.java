package com.atlassian.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private WebElement getUserName() {
        return driver.findElement(By.id("username"));
    }

    private WebElement getPassword() {
        return driver.findElement(By.id("password"));
    }

    private WebElement getSubmit() {
        return driver.findElement(By.id("login-submit"));
    }

    public void login(String userName, String password) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        this.getUserName().sendKeys(userName);
        this.getSubmit().click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        this.getPassword().sendKeys(password);
        this.getSubmit().click();
    }

    public void assertPage() {
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        new WebDriverWait(driver, 20).until(ExpectedConditions.titleIs("Dashboard - Confluence"));
    }
}

package com.atlassian.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreatePage extends BasePage {

    public CreatePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getPagesLink() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[href*='/wiki/spaces/PA/pages']")));
        WebElement pageLink = driver.findElement(By.cssSelector("a[href*='/wiki/spaces/PA/pages']"));
        return pageLink;
    }

    public WebElement getCreatePageButton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='button']")));
        driver.manage().window().maximize();
        WebElement createPageButton = driver.findElement(By.cssSelector(".dwVXlo"));
        return createPageButton;
    }

    public WebElement getBlankPageLink() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".template-select-container li")));
        WebElement blankPageLink = driver.findElement(By.cssSelector(".template-select-container li"));
        return blankPageLink;
    }

    public WebElement getCreateDialogButton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Create']")));
        WebElement createDialogButton = driver.findElement(By.xpath("//button[text()='Create']"));
        return createDialogButton;
    }

    public WebElement getDialog() {
        return driver.findElement(By.cssSelector(".dialog-components"));
    }

    public String getAddPageTitle() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("Add Page - Project Atlassian - Confluence"));
        return driver.getTitle();
    }

}



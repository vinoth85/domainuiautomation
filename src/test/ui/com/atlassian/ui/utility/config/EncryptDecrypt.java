package com.atlassian.ui.utility.config;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Base64;

public class EncryptDecrypt {

    private static String key = "2ZnzK5AhVR2YXxncRTk0MQ==";

    public static String encrypt(String encryptString) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException, IllegalBlockSizeException, BadPaddingException {
        byte[] cipher = createEncrypted(encryptString, StringToSecretKey(key));
        return Base64.getEncoder().encodeToString(cipher);
    }

    public static String decrypt(String encryptedString) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        byte[] decodeedString = Base64.getDecoder().decode(encryptedString);
        return decrypt(decodeedString, StringToSecretKey(key));
    }

    private static String secretKeyToString() throws NoSuchAlgorithmException {
        SecretKey secretKey = getSecretKey();
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    private static SecretKey StringToSecretKey(String encodedKey) {
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }

    private static SecretKey getSecretKey() throws NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());
        SecureRandom random = SecureRandom.getInstanceStrong();
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(128, random);
        return keyGen.generateKey();
    }

    static byte[] createEncrypted(String encryptString, SecretKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException, IllegalBlockSizeException, BadPaddingException {
        byte ivbytes[] = new byte[12];
        new SecureRandom().nextBytes(ivbytes);
        GCMParameterSpec spec = new GCMParameterSpec(128, ivbytes);

        Cipher c = Cipher.getInstance("AES/GCM/NoPadding");
        c.init(Cipher.ENCRYPT_MODE, key, spec);

        assert spec.getIV().length == ivbytes.length;
        assert c.getIV().length == spec.getIV().length;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(spec.getIV());

        byte[] cb;
        cb = c.update("".getBytes(StandardCharsets.UTF_8));
        if (cb != null) bos.write(cb);
        cb = c.doFinal(encryptString.getBytes(StandardCharsets.UTF_8));
        if (cb != null) bos.write(cb);
        bos.close();

        byte[] cipher = bos.toByteArray();

        return cipher;
    }

    static String decrypt(byte[] cipherText, SecretKey key) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        InputStream in = new ByteArrayInputStream(cipherText);

        byte[] ivbytes = readFully(in, 12);
        GCMParameterSpec spec = new GCMParameterSpec(128, ivbytes);

        Cipher c = Cipher.getInstance("AES/GCM/NoPadding");
        c.init(Cipher.DECRYPT_MODE, key, spec);

        CipherInputStream cis = new CipherInputStream(in, c);
        BufferedReader br = new BufferedReader(new InputStreamReader(cis));
        String decrypted = br.readLine();
        cis.close();
        return decrypted;
    }

    private static byte[] readFully(InputStream in, int i) throws IOException {
        byte[] b = new byte[i];
        int len = i;
        int off = 0;
        do {
            int read = in.read(b, off, len);
            off += read;
            len -= read;
        } while (len > 0);
        return b;
    }

}

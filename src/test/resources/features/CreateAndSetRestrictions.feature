@createandsetrestrictions
Feature: Create and Set restrictions to a confluence page

  As a Confluence user
  I want to create a page
  and set restrictions on the page
  so that only certain users can view the page

  Background:
    Given I login to the Confluence Page as "vinothmathivanan1@gmail.com" and "m7BQ5oBibLdypTGWJ4h76LXhIG+eXavTZvdR+0B4GFpmS78a"
    When I navigate to the project atlassian space

  Scenario Outline: Create a page
    And I create a blank page
    And I provide the title "<pageTitle>"
    And I publish the page
    Then I validate the page "<pageTitle>" has created
    And I open the page "TestPage"
    Then I delete the page

    Examples:
      | pageTitle |
      | TestPage  |

  Scenario Outline: Set restrictions to an user on an existing page
    And I create a blank page
    And I provide the title "<pageTitle>"
    And I publish the page
    And I validate the page "<pageTitle>" has created
    And I open the page "TestPage"
    And I edit the restrictions
    Then I set the restrictions "Viewing and editing restricted" to the page for the user "vinoth"
    And I logout
    And I clear the cookies
    And I login to the Confluence Page as "vinothmathivanan@gmail.com" and "m7BQ5oBibLdypTGWJ4h76LXhIG+eXavTZvdR+0B4GFpmS78a"
    When I navigate to the project atlassian space
    And I open the page "TestPage"
    Then I confirm that the page is restricted

    Examples:
      | pageTitle |
      | TestPage  |

  Scenario: Delete the page
    And I open the page "TestPage"
    Then I delete the page



